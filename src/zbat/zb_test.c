#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <termios.h>

#define FRAME_MAX_SIZE 72
void *monitor(void *arg);

void print_packet(const char *buf, const int n) {
	int i;
	printf("[");
	for (i = 0; i < n; i++) {
		printf("%02x ", buf[i]);
	}
	printf("]\n" );
}



void write_bytes(int fd, const char *buf, const int n) {
	int i;

	write(fd, buf, n);
/*	for (i = 0; i < n; i++) {
		write(fd, &buf[i], 1);
	}
*/
}

int main(void) {
	int serial;
	pthread_t mthread;
	int c, s;
	char buf[FRAME_MAX_SIZE], pbuf[FRAME_MAX_SIZE];
	struct termios tc;

	/* open serial port */
	serial = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);
	fcntl(serial, F_SETFL, 0);
	
	/* set tc options for serial port transfers */
	tcgetattr(serial, &tc);

	cfsetospeed(&tc, 9600);
	cfsetispeed(&tc, 9600);

	tc.c_cflag |= (CLOCAL | CREAD);

	tc.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

	/* apply configuration */
	tcsetattr(serial, TCSANOW, &tc);


	pthread_create(&mthread, NULL, monitor, (void *) serial);

	c = 0;
	while (c != 'q') {
		c = getc(stdin);

		switch(c) {
			case 'h':
				write_bytes(serial, "Hello World.", 12);
				printf("sent a message as many 1-character writes.\n");
				break;
			case 'd':
				printf("entering command mode\n");

				usleep(1000*1000);
				write_bytes(serial, "+++", 3);
				usleep(1000*1000);
				write_bytes(serial, "ATNI\r\n", 6);
				
				printf("sent ATNI command\n");
				break;
			case '\n':
				break;
			default:
				printf("invalid command\n");
		}
	}

	printf("attempting to cancel monitor thread\n");
	pthread_cancel(mthread);

	printf("closing port.\n");
	close(serial);

	printf("done.\n");
	return 0;
}

void *monitor(void *arg) {
	int serial;
	int c;

	printf("serial monitoring thread started\n");

	serial = (int) arg;

	while ( read(serial, &c, 1) > 0 ) {
		if ((char) c == 0x7e) {
			fprintf(stderr, "\n");
		}

		fprintf(stderr, "%c", (char) c);

		if ((char) c == 0x0d) {
			fprintf(stderr, "\n");
		}
	}
	return NULL;
}

