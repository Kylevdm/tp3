from bottle import route, run, static_file

@route('/hello/:name')
def index(name='World'):
    return template('<b>Hello {{name}}</b>!', name=name)


@route('/static/<path:path>')
def callback(path):
    return static_file(path, 'static/')


run(host='localhost', port=8080)
